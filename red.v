module red(
	input wire clk,
	input wire input_value,

	output reg output_value
);

	reg ff1_reg, ff1_next;
	reg ff2_reg, ff2_next;

	always @ (posedge clk) begin
			ff1_reg <= ff1_next;
			ff2_reg <= ff2_next;
	end

	always @(*) begin
		ff1_next = input_value;
		ff2_next = ff1_reg;
		
		output_value = ff1_reg & !ff2_reg;
	end

endmodule
