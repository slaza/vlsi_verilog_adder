module main(
	input wire clk,
	input wire reset,
	
	input wire sw0,
	input wire sw1,
	input wire sw2,
	
	input wire b0,
	input wire b1,
	input wire b2,
	
	output wire [6:0] hex0,
	output wire [6:0] hex1,
	output wire [6:0] hex2,
	output wire [6:0] hex3
);

	//za cuvanje ucitane vrednosti
	integer value1_reg, value1_next;
	
	
	integer value_prev1_reg, value_prev1_next;
	integer value_prev2_reg, value_prev2_next;
	
	reg [3:0] cifra0;
	reg [3:0] cifra1;
	reg [3:0] cifra2;
	
	//stanja
	localparam ISKLJUCEN_EKRAN = 3'b000;
	localparam MENJAJ_1 = 3'b001;
	localparam MENJAJ_2 = 3'b010;
	localparam MENJAJ_3 = 3'b011;
	
	localparam SACUVAJ_CIFRU = 3'b100;
	localparam PRIKAZI_CIFRE = 3'b101;
	localparam ZBIR = 3'b110;
	
	reg [2:0] state_reg;
	reg [2:0] state_next;
	
	//detektori uzlazne ivice
	wire red_b0, red_b1, red_b2;
	
	red r0(
		.clk(clk),
		.input_value(b0),
		.output_value(red_b0)
	);
	
	red r1(
		.clk(clk),
		.input_value(b1),
		.output_value(red_b1)
	);
	
	red r2(
		.clk(clk),
		.input_value(b2),
		.output_value(red_b2)
	);
	
	//displeji
	reg [3:0] disp0;
	reg [3:0] disp1;
	reg [3:0] disp2;
	reg [3:0] disp3;
	
	bin_to_ss bin0(disp0, hex0);
	bin_to_ss bin1(disp1, hex1);
	bin_to_ss bin2(disp2, hex2);
	bin_to_ss bin3(disp3, hex3);
	
	//brojac
	parameter JEDNA_SEKUNDA = 50_000_000;
	parameter DVE_SEKUNDE =  2 * JEDNA_SEKUNDA;
	parameter TRI_SEKUNDE = 3 * JEDNA_SEKUNDA;
	parameter CETIRI_SEKUNDE = 4 * JEDNA_SEKUNDA;
	parameter PET_SEKUNDI = 5 * JEDNA_SEKUNDA;
	
	integer timer_reg, timer_next;
	
	//zbir
	integer temp_zbir;
	
	always @ (posedge clk, posedge reset) begin
		if(reset == 1) begin
			//1,2
			value1_reg <= 0;
			state_reg <= ISKLJUCEN_EKRAN;
			//3
			value_prev1_reg <= 0;
			value_prev2_reg <= 0;
			timer_reg <= 0;
		end else begin
			//1,2
			value1_reg <= value1_next;
			state_reg <= state_next;
			//3
			timer_reg = timer_next;
			value_prev1_reg <= value_prev1_next;
			value_prev2_reg <= value_prev2_next;

		end
	end
	
	always @ (*) begin
		//iskljuci displeje
		disp0 = 10;
		disp1 = 10;
		disp2 = 10;
		disp3 = 10;
	
		state_next = state_reg;
		value1_next = value1_reg;
		
		cifra0 = value1_reg % 10;
		cifra1 = (value1_reg / 10) % 10;
		cifra2 = (value1_reg / 100) % 10;
		
		//3
		value_prev1_next = value_prev1_reg;
		value_prev2_next = value_prev2_reg;
		timer_next = timer_reg;
		
		case(state_reg)
			MENJAJ_1:
				//ispis
				begin
					disp0 = cifra0;
					
					
					if(red_b0 == 1) begin
						value1_next = (cifra0 == 9)? value1_reg : value1_reg + 1;
					end
						
						
					if(red_b1 == 1) begin
						state_next = MENJAJ_2;
					end else if(sw0 == 0) begin
						state_next = SACUVAJ_CIFRU;
					end else if(sw1 == 1) begin
						state_next = PRIKAZI_CIFRE;
					end else if(sw2 == 1) begin
						state_next = ZBIR;
					end
				end
				
				MENJAJ_2:
				//ispis
				begin
					disp0 = cifra0;
					disp1 = cifra1;
					
					if(red_b0 == 1) begin
						value1_next = (cifra1 == 9)? value1_reg : value1_reg + 10;
					end
						
						
					if(red_b1 == 1) begin
						state_next = MENJAJ_3;
					end else if(sw0 == 0) begin
						state_next = SACUVAJ_CIFRU;
					end else if(sw1 == 1) begin
						state_next = PRIKAZI_CIFRE;
					end else if(sw2 == 1) begin
						state_next = ZBIR;
					end
				end
				
				MENJAJ_3:
				//ispis
				begin
					disp0 = cifra0;
					disp1 = cifra1;
					disp2 = cifra2;
					
					if(red_b0 == 1) begin
						value1_next = (cifra2 == 9)? value1_reg : value1_reg + 100;
					end 
						
					if(sw0 == 0) begin
						state_next = SACUVAJ_CIFRU;
					end else if(sw1 == 1) begin
						state_next = PRIKAZI_CIFRE;
					end else if(sw2 == 1) begin
						state_next = ZBIR;
					end
				end
			ISKLJUCEN_EKRAN:
				begin
					
					//sledece stanje
					if(sw0 == 1) begin
						state_next = MENJAJ_1;
					end else if(sw1 == 1) begin
						state_next = PRIKAZI_CIFRE;
					end else if(sw2 == 1) begin
						state_next = ZBIR;
					end
				end
				
			SACUVAJ_CIFRU:
				begin
					/*
						dva registra u kojima cuvamo poslednje dve unete vrednosti
					*/
					value_prev2_next = value_prev1_reg;
					value_prev1_next = value1_reg;
					
					value1_next = 0;	//iskljuci novu vrednost
					
					state_next = ISKLJUCEN_EKRAN;
				end
			
			PRIKAZI_CIFRE:
				begin
					
					timer_next = (timer_reg == PET_SEKUNDI)? 0 : timer_reg + 1;
					
					//prikazi cifu 1
					if(timer_reg <= DVE_SEKUNDE) begin
						disp0 = value_prev1_reg % 10;
						disp1 = (value_prev1_reg / 10) % 10;
						disp2 = (value_prev1_reg / 100) % 10;
					
					//prikazi cifru 2
					end else if(timer_reg >= TRI_SEKUNDE) begin
						disp0 = value_prev2_reg % 10;
						disp1 = (value_prev2_reg / 10) % 10;
						disp2 = (value_prev2_reg / 100) % 10;
					end
					
					if(sw1 == 0) begin
						state_next = ISKLJUCEN_EKRAN;
					end
				end
			ZBIR:
				begin
					temp_zbir = value_prev1_reg + value_prev2_reg;
				
					disp0 = (temp_zbir) % 10;
					disp1 = ((temp_zbir) / 10) % 10;
					disp2 = ((temp_zbir) / 100) % 10;
					disp3 = ((temp_zbir) / 1000) % 10;
				
					if(sw2 == 0) begin
						state_next = ISKLJUCEN_EKRAN;
					end
					
				end
				
		endcase
	end
	
endmodule















