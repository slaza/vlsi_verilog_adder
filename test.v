module test();
	reg clk;
	reg reset;
	
	reg button0;
	reg button1;
	reg button2;
	
	reg sw0, sw1, sw2;
	
// 	wire [7:0] led;
	wire [6:0] hex0;
	wire [6:0] hex1;
	wire [6:0] hex2;
	wire [6:0] hex3;
	
	integer error;
	integer index;
	
	defparam m0.JEDNA_SEKUNDA = 4;
	main m0(
		.clk(clk), 
		.reset(reset), 
		
		.sw0(sw0),
		.sw1(sw1),
		.sw2(sw2),
		
		.b0(button0),
		.b1(button1),
		.b2(button2),
		
		.hex0(hex0),
		.hex1(hex1),
		.hex2(hex2),
		.hex3(hex3)
	);
	
	initial begin
		$dumpfile("dump.vcd");
		$dumpvars(0, test);
	
		clk = 0;
		reset = 0;
		button0 = 0;
		button1 = 0;
		button2 = 0;
		sw0 = 0;
		sw1 = 0;
		sw2 = 0;
		
		error = 0;
		index = 0;
	end
	
	always  begin
		#1 clk = 0;
		#1 clk = 1;
	end

	initial begin
		#2 reset = 1;
		#2 reset = 0;
		
		for(index = 0; index < 10; index = index + 1) begin
		
			#2 sw0 = 1;

				//cifra 1
					#4 button0 = 1;
					#4 button0 = 0;
					
					#4 button0 = 1;
					#4 button0 = 0;
				
				//cifra 2
				#4 button1 = 1;
				#4 button1 = 0;
			
					#4 button0 = 1;
					#4 button0 = 0;
					
					#4 button0 = 1;
					#4 button0 = 0;
					
					#4 button0 = 1;
					#4 button0 = 0;
					
					#4 button0 = 1;
					#4 button0 = 0;
					
					#4 button0 = 1;
					#4 button0 = 0;
				

				//cifra 3
				#4 button1 = 1;
				#4 button1 = 0;
				
				#4 button0 = 1;
				#4 button0 = 0;
				
				#4 button0 = 1;
				#4 button0 = 0;
				
				#4 button0 = 1;
				#4 button0 = 0;
				
				#4 button0 = 1;
				#4 button0 = 0;
				
				#4 button0 = 1;
				#4 button0 = 0;
				
				#6 button0 = 1;
				#4 button0 = 0;
				
				#4 button0 = 1;
				#4 button0 = 0;
				
				#4 button0 = 1;
				#4 button0 = 0;
				
				#4 button0 = 1;
				#4 button0 = 0;
				
				#4 button0 = 1;
				#4 button0 = 0;
			//sacuvaj cifru	
			#4 sw0 = 0;
			
			//cifra 2
			#10 sw0 = 1;
			
					#4 button0 = 1;
					#4 button0 = 0;
					
					#4 button0 = 1;
					#4 button0 = 0;
					
					#4 button0 = 1;
					#4 button0 = 0;
				
				//cifra 2
				#4 button1 = 1;
				#4 button1 = 0;
			
					#4 button0 = 1;
					#4 button0 = 0;
					
					#4 button0 = 1;
					#4 button0 = 0;
									
					#4 button0 = 1;
					#4 button0 = 0;
					
					#4 button0 = 1;
					#4 button0 = 0;
					
					#4 button0 = 1;
					#4 button0 = 0;
					
					#4 button0 = 1;
					#4 button0 = 0;
				
					#4 button1 = 1;
					#4 button1 = 0;
					
					#4 button0 = 1;
					#4 button0 = 0;
				//cifra 3
				
				
				#4 button0 = 1;
				#4 button0 = 0;
				
				#4 button0 = 1;
				#4 button0 = 0;
				
				#4 button0 = 1;
				#4 button0 = 0;
				
				#4 button0 = 1;
				#4 button0 = 0;
				
			
			#4 sw0 = 0;
			
			//prikaz oba broja
			#10 sw1 = 1;
			#200 sw1 = 0;
			
			//prikaz zbira
			#10 sw2 = 1;
			
			#20 sw2 = 0;
		end	//for petlja
		
		//reset
		
		#6 reset = 1;
		#4 reset = 0;
		
		#10 $finish;
	end

	
	/*
		proavera gresaka.
		Postoji greska ako error skoci sa 0
	*/
	always @ (*) begin
		if(sw2 == 1) begin
		
		#1 error = error;	//pauza
		
		if(hex0 != 7'h12)
				error = 1;
			else if(hex1 != 7'h7D)
				error = 2;
			else if(hex2 != 7'h12)
				error = 3;
			else if(hex3 != 7'h7D)
				error = 4;
		end
	end
	
endmodule



















